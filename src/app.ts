import Server from "./server";

const server = new Server(4000);


server.start()
    .then(() => console.log(`Running on PORT 4000`))
    .catch(e => {
        console.log(`Starting error: ${e}`);
        process.exit(1);
    });

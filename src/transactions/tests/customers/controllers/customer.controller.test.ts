import {CustomerController} from "../../../customers/controllers/customer.controller";
import {CustomerRepository} from "../../../customers/repositories/customer.repository";

describe("Unit | Model | Customer Repository", () => {

  const repositoryMock = {
    find: () => ([]),
    create: () => ({id: 1}),
    save: () => ({id: 1})
  };
  // @ts-ignore
  const CustomerRepositoryMock = jest.fn<CustomerRepository>(() => ({
    // implementation
    add: jest.fn(),
    find: jest.fn(),
    _repository: repositoryMock
  }));
  describe("when entity loaded", () => {
    const mock = new CustomerRepositoryMock();
    const controller = new CustomerController(mock);
    it("should be an object", () => {
      // Then
      expect(typeof controller).toBe("object");
    });

    it("#getCustomers", () => {
      // Then
      controller.getCustomers({ }, {json: jest.fn()});
      expect( mock.find).toHaveBeenCalled();
    });

    it("#addCustomer", () => {
      // Then
      controller.addCustomer({ body: {name: "test"} }, {json: jest.fn()});
      expect( mock.add).toHaveBeenCalled();
    });
  });
});

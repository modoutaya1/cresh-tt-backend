import {CustomerEntity} from "../../../customers/entities/customer.entity";

describe("Unit | Model | Customer Entity", () => {

  describe("when entity loaded", () => {
    const entity = new CustomerEntity();
    it("should be an object", () => {
      // Then
      expect(typeof entity).toBe("object");
    });

    it("should default undefined", () => {
      // Then
      expect(entity.id).toBe(undefined);
      expect(entity.name).toBe(undefined);
    });

    it("should default not undefined", () => {
      // Then
      expect(entity.created_date).not.toBe(undefined);
    });
  });
});

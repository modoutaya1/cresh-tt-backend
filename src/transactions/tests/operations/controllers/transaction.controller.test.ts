import {TransactionController} from "../../../operations/controllers/transaction.controller";
import {TransactionRepository} from "../../../operations/repositories/transaction.repository";
import {CustomerRepository} from "../../../customers/repositories/customer.repository";
import {TransactionBuilder} from "../../../operations/builders/transaction.builder";

describe("Unit | Model | Customer Repository", () => {

  const repositoryMock = {
    find: () => ([]),
    create: () => ({id: 1}),
    save: () => ({id: 1})
  };

  const transactionObject = {
    "store_name": "store test DDDD",
    "amount": 1800,
    "split": 3,
    "customer": 4
  };
  // @ts-ignore
  const TransactionRepositoryMock = jest.fn<TransactionRepository>(() => ({
    // implementation
    saveTransaction: jest.fn(),
    findTransactionById: jest.fn(),
    findTransactions: jest.fn(),
    findInstalmentByTransaction: jest.fn(),
    _repository: repositoryMock
  }));

  // @ts-ignore
  const CustomerRepositoryMock = jest.fn<CustomerRepository>(() => ({
    // implementation
    findById: jest.fn(),
    _repository: repositoryMock
  }));

  // @ts-ignore
  const TransactionBuilderMock = jest.fn<TransactionBuilder>(() => ({
    // implementation
    createTransaction: () => transactionObject
  }));


  describe("when entity loaded", () => {
    const mock = new TransactionRepositoryMock();
    const mockCustomer = new CustomerRepositoryMock();
    let mockBuilder = new TransactionBuilderMock();
    const controller = new TransactionController(mock, mockCustomer, mockBuilder);
    it("should be an object", () => {
      // Then
      expect(typeof controller).toBe("object");
    });
    //
    it("#getTransaction", () => {
      // Then
      controller.getTransaction({ params: {id: 10}}, {json: jest.fn()});
      expect( mock.findTransactionById).toHaveBeenCalled();
    });

    it("#getTransactions", () => {
      // Then
      controller.getTransactions({}, {json: jest.fn(), query: {}});
      expect( mock.findTransactions).toHaveBeenCalled();
    });

    it("#getInstalments", () => {
      // Then
      controller.getInstalments({params: {id: 10}}, {json: jest.fn(), query: {}});
      expect( mock.findInstalmentByTransaction).toHaveBeenCalled();
    });

    it("#getInstalments", () => {
      // Then
      controller.getInstalments({params: {id: 10}}, {json: jest.fn(), query: {}});
      expect( mock.findInstalmentByTransaction).toHaveBeenCalled();
    });
  });
});

import {TransactionEntity} from "../../../operations/entities/transaction.entity";

describe("Unit | Model | Transaction Entity", () => {

  describe("when entity loaded", () => {
    const entity = new TransactionEntity();
    it("should be an object", () => {
      // Then
      expect(typeof entity).toBe("object");
    });

    it("should default undefined", () => {
      // Then
      expect(entity.id).toBe(undefined);
      expect(entity.customer).toBe(undefined);
      expect(entity.amount).toBe(0);
      expect(entity.customer).toBe(undefined);
    });

    it("should default not undefined", () => {
      // Then
      expect(entity.created_date).not.toBe(undefined);
    });
  });
});

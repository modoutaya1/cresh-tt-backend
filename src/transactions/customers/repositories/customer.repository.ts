import {getRepository, Repository} from "typeorm";
import {CustomerEntity} from "../entities/customer.entity";

class CustomerRepository {

    private _repository: Repository<CustomerEntity>;

    constructor() {
        this._repository = getRepository(CustomerEntity);
    }

    async find() {
        return await this._repository.find();
    }

    async findById(id: number) {
        return await this._repository.findOne(id);
    }

    async add(userJson: any) {
        const user = await this._repository.create(userJson);
        return await this._repository.save(user);
    }
}


export {CustomerRepository};

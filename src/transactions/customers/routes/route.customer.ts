import * as express from 'express';
import * as core from "express-serve-static-core";
import cors from 'cors';

import {CustomerController} from "../controllers/customer.controller";

class RouteCustomer {

    private readonly _router: core.Router;
    private readonly _server: express.Application;
    private readonly _customerCtrl: CustomerController;

    constructor(server: express.Application, controller: CustomerController) {
        this._server = server;
        this._router = express.Router();
        this._router.options('*', cors());
        this._customerCtrl = controller;
    }

    routes() {
        this._router.get("/customers", this._customerCtrl.getCustomers);
        this._router.post("/customers", this._customerCtrl.addCustomer);

        this._server.use('/', this._router)
    }
}

export {RouteCustomer}

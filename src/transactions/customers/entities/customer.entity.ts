"use strict";

import {BaseEntity, Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
class CustomerEntity extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", {length: 255})
    name: string;

    @Column({ type: 'timestamp'})
    created_date: Date = new Date()
}

export {CustomerEntity}

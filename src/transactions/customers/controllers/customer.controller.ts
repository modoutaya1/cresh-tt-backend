import {Request, Response} from "express";
import {CustomerRepository} from "../repositories/customer.repository";

class CustomerController {

    private _customerRepository: CustomerRepository;

    constructor(repo) {
        this._customerRepository = repo;
    }

    getCustomers = async (req, res) => {
        const customerList = await this._customerRepository.find();
        res.json(customerList);
    };

    addCustomer = async (req, res) => {
        const customerCreated  = await this._customerRepository.add(req.body);
        res.json(customerCreated);
    };
}

export {CustomerController};

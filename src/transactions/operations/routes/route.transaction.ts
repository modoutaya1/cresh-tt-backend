import * as express from 'express';
import * as core from "express-serve-static-core";
import cors from 'cors';

import {TransactionController} from "../controllers/transaction.controller";
import {TransactionMiddleware} from "../middlewares/transaction.middleware";

class RouteTransaction {

    private readonly _router: core.Router;
    private readonly _server: express.Application;
    private readonly _transactionCtrl: TransactionController;
    private readonly _transactionMiddleware: TransactionMiddleware;

    constructor(server: express.Application, controller: TransactionController) {
        this._server = server;
        this._router = express.Router();
        this._router.options('*', cors());
        this._transactionCtrl = controller;
        this._transactionMiddleware = new TransactionMiddleware();
    }

    routes() {
        this._router.get("/transactions/:id/instalments", this._transactionMiddleware.idHandler, this._transactionCtrl.getInstalments);
        this._router.put("/transactions/:id/instalments", this._transactionMiddleware.idHandler, this._transactionCtrl.doInstalment);
        this._router.get("/transactions/:id", this._transactionMiddleware.idHandler, this._transactionCtrl.getTransaction);
        this._router.post("/transactions", this._transactionCtrl.addTransaction);
        this._router.get("/transactions", this._transactionCtrl.getTransactions);

      this._router.get("/customers/:id/transactions", this._transactionMiddleware.idHandler, this._transactionCtrl.getTransactionByCustomer);

        this._server.use('/', this._router)
    }
}

export {RouteTransaction}

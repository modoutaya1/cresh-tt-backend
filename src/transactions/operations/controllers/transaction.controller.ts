import * as _ from "lodash";

import {TransactionRepository} from "../repositories/transaction.repository";
import {CustomerRepository} from "../../customers/repositories/customer.repository";
import {TransactionBuilder} from "../builders/transaction.builder";

class TransactionController {

    private _transactionRepository: TransactionRepository;
    private _customerRepository: CustomerRepository;
    private _transactionBuilder: TransactionBuilder;

    constructor(repo: TransactionRepository, customerRepo: CustomerRepository, builder: TransactionBuilder) {
        this._transactionRepository = repo;
        this._transactionBuilder = builder;
        this._customerRepository = customerRepo;
    }

    addTransaction = async (req, res) => {
      const transactionObject = await this._transactionBuilder.createTransaction(req.body);
      if(transactionObject.errors) {
        return res.status(400).json({message: _.get(transactionObject, "errors[0].constraints")})
      }
      const customer = await this._customerRepository.findById(_.get(transactionObject, "customer.id", 0));
      if (!customer) {
        return res.status(404).json({message: "Customer not found"});
      }
      const transactionCreated  = await this._transactionRepository.saveTransaction(transactionObject);
      res.json(transactionCreated);
    };

    getTransaction = async  (req, res) => {
      const transaction  = await this._transactionRepository.findTransactionById(parseInt(req.params.id));
      res.json(transaction);
    };

    getTransactions = async  (req, res) => {
      const transaction  = await this._transactionRepository.findTransactions(req.query);
      res.json(transaction);
    };

    getInstalments = async  (req, res) => {
      const instalments  = await this._transactionRepository.findInstalmentByTransaction(parseInt(req.params.id));
      res.json(instalments);
    };

    doInstalment = async  (req, res) => {
      const transaction  = await this._transactionRepository.doInstalment(parseInt(req.params.id));
      if (!transaction) {
        res.status(404).json({message: "No unpaid  transaction's  instalment  found"})
      }
      res.json(transaction);
    };

    getTransactionByCustomer = async (req, res) => {
      const transactions = await this._transactionRepository.findTransactionByCustomerId(req.params.id, req.query);
      res.json(transactions);
    };
}

export {TransactionController};

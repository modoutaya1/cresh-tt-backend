import * as _ from "lodash";
import {Request, Response} from "express";

class TransactionMiddleware {

  idHandler(req: Request, res: Response, next: Function) {
    const id = _.chain(req).get("params.id").parseInt().value();
    if (!_.isInteger(id) || id <= 0) {
      return res.status(400).json({message: "ID must be positive number"})
    }
    next();
  }
}

export {TransactionMiddleware}

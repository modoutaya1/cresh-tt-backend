"use strict";

import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne
} from "typeorm";

import {TransactionEntity} from "./transaction.entity";

@Entity()
class InstalmentEntity extends BaseEntity{

  @PrimaryGeneratedColumn()
  id: number;

  @Column("float")
  amount: number;

  @Column("boolean")
  is_paid: boolean;

  @Column({ type: 'timestamp', nullable: true})
  planned_date: Date | null = null;

  @Column({ type: 'timestamp', nullable: true})
  paid_date: Date | null = null;

  @ManyToOne(type => TransactionEntity)
  transaction: TransactionEntity;
}

export {InstalmentEntity};

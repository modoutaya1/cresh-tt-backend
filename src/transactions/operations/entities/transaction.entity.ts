"use strict";

import {BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {IsInt, Length, IsNumber, IsDefined, Min} from "class-validator";

import {CustomerEntity} from "../../customers/entities/customer.entity";
import {InstalmentEntity} from "./instalment.entity";

@Entity()
class TransactionEntity extends BaseEntity{

  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", {length: 50})
  @Length(5, 50)
  store_name: string = "";

  @Column("float")
  @IsNumber()
  @Min(1)
  amount: number = 0;

  @Column("int")
  @IsInt()
  split: number = 1;

  @Column("boolean")
  is_completed: boolean = false;

  @Column({ type: 'timestamp'})
  created_date: Date = new Date();

  @ManyToOne(type => CustomerEntity, {cascade: ["insert", "update"], eager: true})
  @IsDefined()
  customer: CustomerEntity;

  @OneToMany(
    type => InstalmentEntity, instalmentEntity => instalmentEntity.transaction,
    {cascade: ["insert", "update"], eager: false })
  instalments: Promise<InstalmentEntity[]>
  // instalments: InstalmentEntity[]

}

export {TransactionEntity}

import {TransactionEntity} from "../entities/transaction.entity";
import {InstalmentEntity} from "../entities/instalment.entity";
import {TransactionUtil} from "../utils/transaction.util";
import {validate} from "class-validator";
import {CustomerEntity} from "../../customers/entities/customer.entity";

class TransactionBuilder {

  async createTransaction(transactionJson: any) {
    const transaction = TransactionUtil.enhance(new TransactionEntity(), transactionJson);
    transaction.created_date = new Date();
    const transactionCustomer = new CustomerEntity();
    transactionCustomer.id = transactionJson.customer;
    transaction.customer = transactionCustomer;
    const errors = await validate(transaction);
    if (errors.length) {
      return {errors};
    }
    const amounts = TransactionUtil.getInstalmentAmounts(transaction);
    transaction.instalments = TransactionBuilder._createInstalment( transaction, amounts);
    return transaction;
  }


  private static _createInstalment(transaction: TransactionEntity, amounts: number[]) {
    return  amounts.map((amount, index) => {
      const instalment = new InstalmentEntity();
      instalment.amount = amount;
      if ( index === 0) {
        instalment.is_paid = true;
        instalment.paid_date = transaction.created_date;
        instalment.planned_date = transaction.created_date
      } else {
        instalment.is_paid = false;
        instalment.paid_date = null;
        instalment.planned_date = TransactionUtil.addMonths(transaction.created_date, index);
      }
      return instalment;
    });
  }
}


export {TransactionBuilder};

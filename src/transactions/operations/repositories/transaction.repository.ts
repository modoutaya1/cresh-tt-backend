import {getRepository, Repository} from "typeorm";

import {TransactionEntity} from "../entities/transaction.entity";
import {InstalmentEntity} from "../entities/instalment.entity";
import {TransactionUtil} from "../utils/transaction.util";
import * as _ from "lodash";

class TransactionRepository {

    private _transactionRepository: Repository<TransactionEntity>;
    private _instalmentEntityRepository: Repository<InstalmentEntity>;

    constructor() {
        this._transactionRepository = getRepository(TransactionEntity);
        this._instalmentEntityRepository = getRepository(InstalmentEntity);
    }

    async findTransactionById(id: number) {
        return await this._transactionRepository.findOne(id);
    }

    // @ts-ignore
  async findTransactions({page = 0, size = 10}) {
      return await this._transactionRepository.find({
        order: { "created_date": "ASC"},
        skip: page,
        take: size
      });
    }

    async findInstalmentByTransaction(id: number) {
      const  transaction = new TransactionEntity();
      transaction.id = id;
      return  await this._instalmentEntityRepository.find({
        where: [{transaction: transaction}],
        order: { "planned_date": "ASC"}
      });
    }

    async saveTransaction(entity: TransactionEntity) {
        const transactionCreated = await this._transactionRepository.save(entity);
        return TransactionRepository._formatTransaction(transactionCreated);
    }

    /**
     *
     * @param id
     */
    async doInstalment(id: number) {
      const transaction = await this._transactionRepository.findOne(id);
      if (!transaction) {
        return ;
      }
      const instalments = await transaction.instalments;
      const nextInstalment = TransactionUtil.getNextInstalment(instalments);
      if (!nextInstalment) {
        return ;
      }
      nextInstalment.is_paid = true;
      nextInstalment.paid_date = new Date();
      if (TransactionUtil.isTransactionComplete(transaction)) {
        transaction.is_completed = true;
      }
      const updateTransaction =  await this._transactionRepository.save(TransactionUtil.updateInstalment(transaction, nextInstalment));
      return TransactionRepository._formatTransaction(updateTransaction);
    }

  // @ts-ignore
  async findTransactionByCustomerId(id, {page= 0, size= 10}) {
    return await this._transactionRepository.find({
      where: [{customer: {id: id}}],
      order: { "created_date": "ASC"},
      skip: page,
      take: size
    });
  }

    private static async _formatTransaction(transaction: TransactionEntity) {
      return _.extend(
        {},
        _.omit(transaction, ["__instalments__", "__has_instalments__"]),
        {instalments: await transaction.instalments}
      );
    }
}


export {TransactionRepository};

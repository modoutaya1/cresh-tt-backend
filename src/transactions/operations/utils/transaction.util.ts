import {TransactionEntity} from "../entities/transaction.entity";
import * as _ from "lodash";
import {InstalmentEntity} from "../entities/instalment.entity";

class TransactionUtil {

  static  enhance = (entity: any, values: any) => _.assign(entity, _.pick(values, _.keys(entity)));

  static getInstalmentAmounts = (transaction: TransactionEntity) => {
    const amount = transaction.amount;
    const split = transaction.split;
    const amounts = Array(split);

    const quotient = Math.floor(amount/ split);
    const remainder = amount % split;

    amounts[0] = quotient + remainder;

    return amounts.fill(quotient, 1);
  };

  static addMonths = (aDate: Date, months: number) => {
    const clonedDate = _.cloneDeep(aDate);
    return new Date(clonedDate.setMonth(clonedDate.getMonth() + months))
  };

  static getNextInstalment = (instalments: InstalmentEntity[]) => {
    return _
      .chain(instalments)
      .filter({is_paid: false})
      .orderBy(['planned_date'], ['asc'])
      .head()
      .value();
  };

  static updateInstalment = (transaction: any, instalment: InstalmentEntity) => {
    const index = _.findIndex(transaction.instalments, {id: instalment.id});
    transaction.instalments[index] = instalment;
    return transaction;
  };

  static isTransactionComplete(transaction: TransactionEntity) {
    return _
      .chain(transaction)
      .get("instalments")
      .every({is_paid: true})
      .value();
  }
}

export {TransactionUtil};

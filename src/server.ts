"use strict";

import express from "express";
import bodyParser from "body-parser";
import swaggerUi from "swagger-ui-express";
import * as swaggerDocument from '../swagger.json';
import {createConnection} from "typeorm";

import {RouteCustomer} from "./transactions/customers/routes/route.customer";
import {CustomerController} from "./transactions/customers/controllers/customer.controller";
import {CustomerRepository} from "./transactions/customers/repositories/customer.repository";

import {RouteTransaction} from "./transactions/operations/routes/route.transaction";
import {TransactionController} from "./transactions/operations/controllers/transaction.controller";
import {TransactionRepository} from "./transactions/operations/repositories/transaction.repository";
import {TransactionBuilder} from "./transactions/operations/builders/transaction.builder";

export default class Server {

    readonly port: number;
    app: express.Application;

    constructor(port: number) {
        this.port = port;
        this.app = express();
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
        this.app.listen(this.port);
    }

    async start() {
        await createConnection();
        new RouteCustomer(this.app, new CustomerController(new CustomerRepository())).routes();
        new RouteTransaction(this.app, new TransactionController(new TransactionRepository(), new CustomerRepository(), new TransactionBuilder())).routes();
    }
}


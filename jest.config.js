module.exports = {
  displayName: {
    name: "CRESH API",
    color: "blue"
  },
  transform: {
    "^.+\\.ts$": "ts-jest"
  },
  testEnvironment: "node",
  testMatch: [
    "**/tests/**/*.test.ts"
  ],
  moduleFileExtensions: [
    "ts", "js"
  ],
  setupFilesAfterEnv: ["<rootDir>/src/transactions/tests/setup"],
  verbose: true,
  bail: false,
  clearMocks: true,
  collectCoverage: true,
  // roots: [
  //   "<rootDir>"
  // ],
  cache: false,
  collectCoverageFrom: [
    "<rootDir>/src/transactions/**/*.ts"
  ],
  coverageThreshold: {
    "global": {
      "branches": 100,
      "functions": 100,
      "lines": 100,
      "statements": 100
    }
  }
};

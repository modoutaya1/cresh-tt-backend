FROM node:12

RUN mkdir -p /var/www/app
WORKDIR /var/www/app

COPY package.json .
RUN  npm install

# Install code
COPY . .
COPY ormconfig.docker.json ./ormconfig.json

EXPOSE 3000 9229

CMD ["npm", "run", "start"]

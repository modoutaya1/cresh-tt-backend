<p style="text-align: center; margin: 40px auto;"><img src="images/logo.png" width="150px" /></p>

# Cresh Backend Engineering Technical Tests - Javascript Flavor

This is a little challenge to help us assess your skills as a developer.

A particular attention will be taken to your Code Structure, your ability to implement the data model and organize the whole project.

## Context

At Cresh we provide credit at shopping cart, instantly.
We aim to ease the way consumers can get the products they want from any shop, both online and instore.

To do so, we provide several apps, both B2B and B2C along with some APIs.

## Prerequisites
* Node.js - https://nodejs.org/en/
* Docker compose  - https://docs.docker.com/compose/reference/overview/


## Install Instructions
1. Download and install the prerequisites
2. Clone your gitlab repository
3. Use the command line to navigate to the project folder
4. Run <code>npm install</code> to install some dependencies to kickstart the project (Koa with Apollo Server support).

## Run Application ([Swagger](http://localhost:4000))
a). Docker
```bash
$ docker-compose build
$ docker-compose up
```
or 
```bash
$ docker-compose up --build
```
b). Local
```bash
$ docker container start cresh_db
$ npm run start
```
## API Documentation
Afta running app go to [Swagger](http://localhost:4000/swagger/)

### Data model

> Note: Relations between tables and default values are voluntarily left out for you to guess.

#### customers

Name | Type | Description
-|-|-
`id` | `integer` | Primary Key
`name` | `varchar(255)` | Name of the customer
`created_date` | `date` | Date of creation

#### transactions

Name | Type | Description
-|-|-
`id` | `integer` | Primary Key
`store_name` | `varchar(50)` | Name of the store where the transaction was made
`amount` | `integer` | Transaction's full amount, in cents
`split` | `integer` | Number of instalments for the transaction
`is_completed` | `boolean` | Have all instalments been paid off ?
`created_date` | `date` | Date of creation

#### instalments

Name | Type | Description
-|-|-
`id` | `integer` | Primary Key
`amount` | `integer` | Instalment amount, in cents
`is_paid` | `boolean` | Is the payment done yet
`planned_date` | `date` | Planned date of payment
`paid_date` | `date` | Actual payment date

### API Documentation

Route | Method | Query String | Body | Description
-|-|-|-|-
`/ping` | GET | - | - | See if the service is running properly, including the DB
`/customers` | GET | - | - | List of customers
`/customers` | POST | - | `name` | Create a customer
`/customers/:id/transactions` | GET | - | - | List of transactions for a specific customer
`/transactions` | POST | - | `store_name`<br />`amount`<br />`split`<br />`user_id` | Create a transaction
`/transactions/:id/instalments` | GET | - | - | List of a transaction's instalments
`/transactions/:id/instalments` | PUT | - | - | Trigger the payment of the transaction's next instalment


## What's evaluated

- Cleanliness & structure of the code
- Code extensibility
- Documentation
- Respect of KISS and DRY principles
- Use of git commits

## Bonus points

- Writing unittests
- Using Typescript
- Using containers for both the api and the database

## Deliverable

Please **clone** this repository and send us a zip or a link to your repo.

We expect :

- A clean files structure
- Some commands to run from the `package.json`
- A documentation on how to install, start and use this API



**Good luck and have fun !**

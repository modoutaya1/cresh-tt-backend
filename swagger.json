{
  "openapi": "3.0.1",
  "info": {
    "title": "Swagger Cresh",
    "description": "API Documentation for Cresh Platform",
    "contact": {
      "email": "modoutaya@gmail.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "version": "1.0.0"
  },
  "externalDocs": {
    "description": "Find out more about Swagger",
    "url": "http://swagger.io"
  },
  "servers": [
    {
      "url": "/",
      "description": "Local Dev"
    },
    {
      "url": "/",
      "description": "With docker-compose"
    }
  ],
  "tags": [
    {
      "name": "Transactions",
      "description": "Access to Cresh transactions"
    },
    {
      "name": "Customers",
      "description": "Operations about customers"
    }
  ],
  "paths": {
    "/transactions": {
      "post": {
        "tags": [
          "Transactions"
        ],
        "summary": "Create a transaction",
        "requestBody": {
          "description": "Transaction information",
          "content": {
            "*/*": {
              "schema": {
                "$ref": "#/components/schemas/TransactionToCreate"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Transaction"
                }
              }
            }
          },
          "400": {
            "description": "Invalid Transaction",
            "content": {}
          },
          "404": {
            "description": "Customer not found",
            "content": {}
          }
        },
        "x-codegen-request-body-name": "body"
      }
    },
    "/transactions/{id}/instalments": {
      "get": {
        "tags": [
          "Transactions"
        ],
        "summary": "Find instalments by transactions ID",
        "description": "List of a transaction's instalments",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of transaction",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Instalment"
                  }
                }
              }
            }
          },
          "404": {
            "description": "Transaction not found",
            "content": {}
          }
        }
      },
      "put": {
        "tags": [
          "Transactions"
        ],
        "summary": "Trigger the payment of the transaction's next instalment",
        "description": "List of a transaction's instalments",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of transaction",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/TransactionWithInstalment"
                }
              }
            }
          },
          "404": {
            "description": "Transaction not found",
            "content": {}
          }
        }
      }
    },
    "/customers": {
      "post": {
        "tags": [
          "Customers"
        ],
        "summary": "Create customer",
        "description": "Create a customer",
        "requestBody": {
          "description": "Created customer object",
          "content": {
            "*/*": {
              "schema": {
                "$ref": "#/components/schemas/Customer"
              }
            }
          },
          "required": true
        },
        "responses": {
          "default": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/Customer"
                }
              }
            }
          }
        },
        "x-codegen-request-body-name": "body"
      },
      "get": {
        "tags": [
          "Customers"
        ],
        "summary": "get List of customers",
        "description": "List of customers",
        "parameters": [

        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Customer"
                  }
                }
              }
            }
          },
          "404": {
            "description": "Transaction not found",
            "content": {}
          }
        }
      }
    },
    "/customers/{id}/transactions": {
      "get": {
        "tags": [
          "Customers"
        ],
        "summary": "get List of transactions for a specific customer",
        "description": "List of transactions for a specific customer",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of customer",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Transaction"
                  }
                }
              }
            }
          },
          "404": {
            "description": "Customer not found",
            "content": {}
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Transaction": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "format": "int64"
          },
          "store_name": {
            "type": "string",
            "description": "Name of the store where the transaction was made"
          },
          "amount": {
            "type": "number"
          },
          "split": {
            "type": "integer"
          },
          "is_completed": {
            "type": "boolean",
            "default": false,
            "description": "transaction Status"
          },
          "created_date": {
            "type": "string",
            "format": "date-time"
          },
          "customer": {
            "type": "object",
            "$ref": "#/components/schemas/Customer"
          }
        },
        "xml": {
          "name": "Transaction"
        }
      },
      "TransactionWithInstalment": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "format": "int64"
          },
          "store_name": {
            "type": "string",
            "description": "Name of the store where the transaction was made"
          },
          "amount": {
            "type": "number"
          },
          "split": {
            "type": "integer"
          },
          "is_completed": {
            "type": "boolean",
            "default": false,
            "description": "transaction Status"
          },
          "created_date": {
            "type": "string",
            "format": "date-time"
          },
          "customer": {
            "type": "object",
            "$ref": "#/components/schemas/Customer"
          },
          "instalments": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Instalment"
            }
          }
        },
        "xml": {
          "name": "Transaction"
        }
      },
      "TransactionToCreate": {
        "type": "object",
        "properties": {
          "store_name": {
            "type": "string",
            "description": "Name of the store where the transaction was made"
          },
          "amount": {
            "type": "number"
          },
          "split": {
            "type": "integer"
          },
          "customer": {
            "type": "integer"
          }
        },
        "xml": {
          "name": "Transaction"
        }
      },
      "Instalment": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "format": "int64"
          },
          "amount": {
            "type": "number"
          },
          "is_paid": {
            "type": "boolean",
            "default": false,
            "description": "Is the payment done yet"
          },
          "planned_date": {
            "type": "string",
            "format": "date-time",
            "description": "Planned date of payment"
          },
          "paid_date": {
            "type": "string",
            "format": "date-time",
            "description": "Actual payment date"
          }
        },
        "xml": {
          "name": "Instalment"
        }
      },
      "Customer": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "format": "int64"
          },
          "name": {
            "type": "string"
          },
          "created_date": {
            "type": "string",
            "format": "date-time",
            "description": "Date of creation"
          }
        },
        "xml": {
          "name": "Customer"
        }
      }
    }
  }
}
